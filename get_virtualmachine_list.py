from pysphere import VIServer, MORTypes
import getpass, sys
from time import gmtime, strftime
curdatestring = strftime("%Y%m%d", gmtime())
import csv
import pprint
import ConfigParser

#Read entries from configuration file
config = ConfigParser.ConfigParser()
config.read("global_config.txt")
username = config.get('global_config', 'username')
password = config.get('global_config', 'password')
hostnames = config.get('global_config', 'vcenter_host')
reportfile = config.get('global_config', 'reporting_path')


def process_hosts(hostname):
    # setup connection to esx host
    server = VIServer()
    try:
        server.connect(hostname, username, password)
    except Exception, e:
        print("ERROR: %s" % e)
        sys.exit(1)

    # setup the csv file and headers
    csvfile = str(reportfile) + str(hostname) + " - vm_report_" + curdatestring + ".csv"
    try:
        outputfile = open(csvfile, 'wb')
    except Exception, e:
        print("Error! %s" % e)

    wr = csv.writer(outputfile, dialect='excel')
    headers = ['Virtual Machine', 'Organizational Folder', 'Powered State', 'OS Type', 'Path on Disk', 'IP Address', 'CPUs', 'Memory', 'Total Disk Space']
    wr.writerow(headers)

    # get a list of all virtual machines on the remote host
    # massive list of all the actual configs from vmware (for 5.1)
    # http://pubs.vmware.com/vsphere-51/index.jsp#com.vmware.wssdk.apiref.doc/vim.VirtualMachine.html
    all_vms = server._get_managed_objects(MORTypes.VirtualMachine, from_mor=None)
    print("DEBUG: Processing %s VM's" % len(all_vms))
    # process the list of virtual machines
    for key in all_vms:
        vmname = all_vms[key]
        alldata = []

        vm1 = server.get_vm_by_name(all_vms[key])
        # uncomment this out to see all the values you can return
        #pprint.pprint(vm1.get_properties())
        #sys.exit(0)

        # i've separated these into initial variables in case work needs to be done with them before being wrote to the csv
        try:
            guest_os = vm1.get_properties()['guest_full_name']
        except Exception, e:
            guest_os = "Unknown"
            print("Failure to gather guest_os, setting to Unknown: %s" % e)
            pass

        ip_address = vm1.get_property('ip_address', from_cache=False)
        local_disk_path = vm1.get_property('path', from_cache=False)
        vmcpus = vm1.get_property('num_cpu')
        vmmemory = vm1.get_property('memory_mb')

        # get folder structure
        folderdata = vm1.properties
        foldername = ""
        lastfolder = ""
        folderpath = ""
        while foldername != "Datacenters":
            # this nasty piece of shit is basically pysphere shitting itself if the virtual machine is actually part
            # of a "VApp". For some reason it doesn't read the parent.name values correctly within it. I'm assuming
            # they are a part of the vapp itself, and not the vm's inside it.
            try:
                foldername = folderdata.parent.name
            except Exception, e:
                foldername = False
            if foldername == False:
                break
            folderdata = folderdata.parent
            fullfolder = foldername +"\\"+ lastfolder
            lastfolder = fullfolder
            folderpath = lastfolder + all_vms[key]
        
        device_list = vm1.get_property('devices')
        totalvdiskspace = 0
        try:
            for key, value in device_list.items():
                if (value)['type'] == "VirtualDisk":
                    # uncomment this if you want to see how big each of the virtual disks is as you go.
                    #print("Label: %s | Size: %s" % ( (value)['label'], (value)['capacityInKB']))
                    totalspacekb = int((value)['capacityInKB']) / 1024
                    totalvdiskspace = totalvdiskspace + totalspacekb
        except Exception, e:
            pass

        folderpath = folderpath.replace('vm\\', '')

        # add all this fun data to the list for csv output
        alldata.append(vmname)
        alldata.append(folderpath)
        alldata.append(vm1.get_status(basic_status=True))
        alldata.append(guest_os)
        alldata.append(local_disk_path)
        alldata.append(ip_address)
        alldata.append(vmcpus)
        alldata.append(vmmemory)
        alldata.append(totalvdiskspace)
        
        wr.writerow(alldata)
        del alldata
    # disconnect from the host, because nobody likes a clingy bitch.
    server.disconnect()

for vcenterhost in hostnames.split(','):
    print("Processing Host: %s" % vcenterhost)
    process_hosts(vcenterhost.replace(' ', ''))