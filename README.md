I wrote this tool while working at Adknowledge to run reports against our Virtualization Platform.

This tool utilizes pysphere to connect into vsphere and grab data out.

Modules Used:
 - [pysphere](http://code.google.com/p/pysphere/)
 - sys
 - time
 - csv
 - pprint
 - ConfigParser

This code has been tested on Python 2.7.x using Windows 7, Debian Wheezy, CentOS 5.x and 6.x.

